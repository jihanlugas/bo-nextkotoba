module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx}',
    './src/components/**/*.{js,ts,jsx,tsx}',
    './src/stores/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {},
    fontFamily: {
      inter: ['Inter', 'sans-serif'],
      notoSerif: ['Noto Serif JP', 'serif'],
			yujiSyuku: ['Yuji Syuku', 'serif'],
    },
  },
  plugins: [],
};
