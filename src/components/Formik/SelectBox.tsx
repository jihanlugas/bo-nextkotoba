import { FastField, ErrorMessage } from 'formik';
import { NextPage } from 'next';

interface Props {
	label?: string;
	name: string;
	listData: any[];
	dataLabel: string;
	dataValue: string;
	required?: boolean;
	placeholder?: string;
}

const SelectBox: React.FC<Props & React.HTMLProps<HTMLSelectElement>> = ({ label, name, listData, dataLabel, dataValue, placeholder, required, ...props }) => {
	return (
		<div className={'flex flex-col w-full'}>
			{label && (
				<div className={''}>
					<span>{label}</span>
					{required && <span className={'text-red-600'}>{'*'}</span>}
				</div>
			)}
			<FastField
				className={'w-full border-2 rounded h-10 px-2 bg-gray-50 disabled:bg-gray-200 disabled:cursor-not-allowed'}
				as={'select'}
				name={name}
				{...props}
			>
				{placeholder && (
					<option value={''}>{placeholder}</option>
				)}
				{listData.map((v, key) => (
					<option key={key} value={v[dataValue]}>{v[dataLabel]}</option>
				))}
			</FastField>
			<ErrorMessage name={name}>
				{(msg) => {
					return (
						<div className={'text-red-600 text-sm normal-case'}>{msg}</div>
					);
				}}
			</ErrorMessage>
		</div>
	);
};

export default SelectBox;