import '../styles/globals.css';
import type { AppProps } from 'next/app';
import Head from 'next/head';
import PageWithLayoutType from '@type/layout';
import { QueryClient, QueryClientProvider } from 'react-query';
import { NotifContextProvider } from '@stores/notifProvider';
import { UserContextProvider } from '@stores/userProvider';

type AppLayoutProps = {
  Component: PageWithLayoutType
  pageProps: any
}


const MyApp: React.FC<AppLayoutProps> = ({ Component, pageProps }) => {

  const Layout = Component.layout || (({ children }) => <>{children}</>);
  const queryClient = new QueryClient();

  return (
    <>
      <Head>
        {/* <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="title" content={process.env.APP_NAME} />
        <meta name="description" content={process.env.APP_NAME} />
        <meta name="og:title" content={process.env.APP_NAME} />
        <meta name="og:description" content={process.env.APP_NAME} /> */}
        <title>{process.env.APP_NAME}</title>
      </Head>
      <NotifContextProvider>
        <UserContextProvider>
          <QueryClientProvider client={queryClient}>
            <Layout>
              <Component {...pageProps} />
            </Layout>
            {/* <ReactQueryDevtools /> */}
          </QueryClientProvider>
        </UserContextProvider>
      </NotifContextProvider>
    </>
  );
};

export default MyApp;
