import Main from '@com/Layout/Main';
import { Api } from '@lib/Api';
import type { Word } from '@type/data';
import PageWithLayoutType from '@type/layout';
import Head from 'next/head';
import { GetServerSideProps } from 'next/types';
import * as Yup from 'yup';
import { Form, Formik, FormikValues, FieldArray } from 'formik';
import { WORD_TAG } from '@utils/Constant';
import { CgChevronRight } from 'react-icons/cg';
import TextField from '@com/Formik/TextField';
import SelectBox from '@com/Formik/SelectBox';
import { IoAddOutline, IoCloseOutline } from 'react-icons/io5';
import ButtonSubmit from '@com/Formik/ButtonSubmit';
import { useMutation } from 'react-query';
import { useRouter } from 'next/router';
import { useContext } from 'react';
import NotifContext from '@stores/notifProvider';
import CheckBox from '@com/Formik/CheckBox';

type Props = {
  word: Word
}

const schema = Yup.object().shape({
  word: Yup.string().matches(/^[一-龯ぁ-んァ-ン]+$/, 'field word').required('required field'),
  wordTag: Yup.string().oneOf(Object.keys(WORD_TAG)).required(),
  listWordRuby: Yup.array().of(
    Yup.string()
      .matches(/^[ぁ-んァ-ン.。\s|-]+$/, 'field kana')
  ),
  listMeaning: Yup.array().of(
    Yup.string()
      .required('required field')
  ),
  listKanji: Yup.array().of(
    Yup.string()
      .required('required field')
  ),
  isSingleruby: Yup.boolean(),
  isCommon: Yup.boolean(),
});

const Edit: React.FC<Props> = ({ word }) => {

  const router = useRouter();

  const { notif } = useContext(NotifContext);

  const { data: dataSubmit, mutate: mutateSubmit, isLoading: isLoadingSubmit } = useMutation((val: FormikValues) => Api.post('/word/update', val));

  const handleSubmit = (values: FormikValues, setErrors) => {
    mutateSubmit(values, {
      onSuccess: (res) => {
        if (res) {
          if (res.success) {
            notif.success(res.message);
            router.push('/word');
          } else if (res.error) {
            if (res.payload && res.payload.listError) {
              setErrors(res.payload.listError);
            } else {
              notif.error(res.message);
            }
          }
        }
      },
      onError: (res) => {
        notif.error('Please cek you connection');
      },
    });
  };

  return (
    <>
      <Head>
        <title>{word.word + ' - Word'}</title>
      </Head>
      <div className='px-4'>
        <div className='text-xl h-16 flex items-center border-b'>
          <button type='button' className='' onClick={() => router.push({ pathname: '/word' })}>{'Word'}</button>
          <CgChevronRight className='mx-2' />
          <button type='button' className='' onClick={() => router.push({ pathname: '/word/[word]', query: { word: word.word } })}>{word.word}</button>
          <CgChevronRight className='mx-2' />
          <span className='' >{'Edit'}</span>
        </div>
        <div className='pt-4'>
          <Formik
            initialValues={word}
            validationSchema={schema}
            enableReinitialize={true}
            onSubmit={(values, { setErrors }) => handleSubmit(values, setErrors)}
          >
            {({ values, setValues }) => {
              return (
                <Form>
                  <div className='flex mb-4'>
                    <div className={'w-full md:w-2/3'}>
                      <div className="mb-4">
                        <TextField
                          label={'Word'}
                          name={'word'}
                          type={'text'}
                          placeholder={'Word'}
                          required
                        />
                      </div>
                      <div className="mb-4">
                        <SelectBox
                          label={'Word Tag'}
                          name={'wordTag'}
                          listData={Object.values(WORD_TAG)}
                          dataLabel={'label'}
                          dataValue={'key'}
                          placeholder={'Select'}
                          required
                        />
                      </div>
                      <div className="mb-4">
                        <CheckBox
                          name={'isSingleruby'}
                          label={'Single Ruby'}
                          id={'is-singleruby'}
                        />
                      </div>
                      <div className="mb-4">
                        <CheckBox
                          name={'isCommon'}
                          label={'Common Word'}
                          id={'is-common'}
                        />
                      </div>
                      <div className="mb-4">
                        <FieldArray
                          name='listWordRuby'
                          render={arrayHelpers => {
                            return (
                              <div>
                                <div>{'List Word Tag Ruby'}</div>
                                <div className='border p-2'>
                                  <div className='grid grid-cols-1 md:grid-cols-3 gap-4 mb-4'>
                                    {values.listWordRuby.map((v, key) => (
                                      <div className="flex" key={key}>
                                        <TextField
                                          name={`listWordRuby.${key}`}
                                          type={'text'}
                                          placeholder={'Word Ruby'}
                                        />
                                        <button type='button' className={'h-10 w-10 flex justify-center items-center '} onClick={() => arrayHelpers.remove(key)}>
                                          <IoCloseOutline className={'text-red-500'} size={'1.5rem'} />
                                        </button>
                                      </div>
                                    ))}
                                  </div>
                                  <div className='flex justify-center'>
                                    <div className='px-4 select-none'>
                                      <button
                                        className={'duration-300 h-10 font-semibold w-full flex justify-center items-center text-purple-400 hover:text-purple-500'}
                                        onClick={() => arrayHelpers.push('')}
                                        type={'button'}
                                      ><IoAddOutline className='mr-2' size={'1.5em'} /> {'Add'}</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            );
                          }}
                        />
                      </div>
                      <div className="mb-4">
                        <FieldArray
                          name='listMeaning'
                          render={arrayHelpers => {
                            return (
                              <div>
                                <div>{'List Meaning'}</div>
                                <div className='border p-2'>
                                  <div className='grid grid-cols-1 md:grid-cols-3 gap-4 mb-4'>
                                    {values.listMeaning.map((v, key) => (
                                      <div className="flex" key={key}>
                                        <TextField
                                          name={`listMeaning.${key}`}
                                          type={'text'}
                                          placeholder={'Meaning'}
                                        />
                                        <button type='button' className={'h-10 w-10 flex justify-center items-center '} onClick={() => arrayHelpers.remove(key)}>
                                          <IoCloseOutline className={'text-red-500'} size={'1.5rem'} />
                                        </button>
                                      </div>
                                    ))}
                                  </div>
                                  <div className='flex justify-center'>
                                    <div className='px-4 select-none'>
                                      <button
                                        className={'duration-300 h-10 font-semibold w-full flex justify-center items-center text-purple-400 hover:text-purple-500'}
                                        onClick={() => arrayHelpers.push('')}
                                        type={'button'}
                                      ><IoAddOutline className='mr-2' size={'1.5em'} /> {'Add'}</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            );
                          }}
                        />
                      </div>
                      <div className={'mb-4'}>
                        <ButtonSubmit
                          label={'Save'}
                          disabled={isLoadingSubmit}
                          loading={isLoadingSubmit}
                        />
                      </div>
                    </div>
                    <div className="hidden md:flex w-1/3 mb-4 p-4 whitespace-pre-wrap">
                      {JSON.stringify(values, null, 4)}
                    </div>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </div>
      </div>
    </>
  );
};

(Edit as PageWithLayoutType).layout = Main;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { word } = context.query;
  const data = await Api.get('/word/word/' + word).then(res => res);

  if (data.success) {
    return {
      props: {
        word: data.payload,
      }
    };
  } else {
    return {
      notFound: true
    };
  }
};

export default Edit;