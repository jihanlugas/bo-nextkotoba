import Head from 'next/head';
import Main from '@com/Layout/Main';
import PageWithLayoutType from '@type/layout';
import { MdOutlineFilterList } from 'react-icons/md';
import { RiPencilLine } from 'react-icons/ri';
import { IoAddOutline } from 'react-icons/io5';
import type { Word, PageInfo, PageRequest } from '@type/data';
import Link from 'next/link';
import { useState, useEffect, PropsWithChildren } from 'react';
import Table from '@com/Table/Table';
import { WORD_TAG } from '@utils/Constant';
import { Api } from '@lib/Api';
import { useQuery } from 'react-query';
import { CellProps, Column } from 'react-table';

type Props = {
}

const column: Column[] = [
  {
    id: 'listWordRuby',
    Header: 'Word',
    Cell: (cell: PropsWithChildren<CellProps<Word, any>>) => {
      return (
        <>
          <div className='flex flex-wrap text-lg font-notoSerif items-end whitespace-nowrap'>
            {cell.row.original.isSingleruby ? (
              <>
                <ruby className='whitespace-nowrap'>
                  {cell.row.original.word}<rt className='font-semibold'>{cell.row.original.listWordRuby[0]}</rt>
                </ruby>
              </>
            ) : (
              <>
                {cell.row.original.word.split('').map((data, key) => (
                  <ruby key={key} className={'whitespace-nowrap'}>
                    {data}<rt className='font-semibold' >{cell.row.original.listWordRuby[key]}</rt>
                  </ruby>
                ))}
              </>
            )}
          </div>
        </>
      );
    },
  },
  {
    id: 'wordTag',
    Header: 'Word Tag',
    Cell: (cell: PropsWithChildren<CellProps<Word, any>>) => {
      return (
        <>
          <span className='whitespace-nowrap'>{WORD_TAG[cell.row.original.wordTag].label}</span>
        </>
      );
    },
  },
  {
    id: 'listMeaning',
    Header: 'List Word Meaning',
    Cell: (cell: PropsWithChildren<CellProps<Word, any>>) => {
      return (
        <>
          <div className='flex flex-col'>
            {cell.row.original.listMeaning.map((data, key) => (
              <div className='mb-2' key={key}>
                {data}
              </div>
            ))}
          </div>
        </>
      );
    },
  },
  {
    id: 'listKanji',
    Header: 'List Kanji',
    Cell: (cell: PropsWithChildren<CellProps<Word, any>>) => {
      return (
        <>
          <div className='font-notoSerif'>
            {cell.row.original.listKanji.join(', ')}
          </div>
        </>
      );
    },
  },
  {
    id: 'isCommon',
    Header: 'Common Word',
    Cell: (cell: PropsWithChildren<CellProps<Word, any>>) => {
      return (
        <>
          <span className='whitespace-nowrap'>{cell.row.original.isCommon ? 'Yes' : 'No'}</span>
        </>
      );
    },
  },
  {
    id: 'wordId',
    Header: '',
    Cell: (cell: PropsWithChildren<CellProps<Word, any>>) => {
      return (
        <>
          <div className='flex justify-end'>
            <Link href={{ pathname: '/word/[word]', query: { word: cell.row.original.word } }}>
              <a>
                <div className='ml-2 px-2 duration-300 text-purple-400 hover:text-purple-500 '>
                  <MdOutlineFilterList className='' size={'1.5rem'} />
                </div>
              </a>
            </Link>
            <Link href={{ pathname: '/word/[word]/edit', query: { word: cell.row.original.word } }}>
              <a>
                <div className='ml-2 px-2 duration-300 text-purple-400 hover:text-purple-500 '>
                  <RiPencilLine className='' size={'1.5rem'} />
                </div>
              </a>
            </Link>
          </div>
        </>
      );
    }
  }
];


const Index: React.FC<Props> = () => {
  const [words, setWords] = useState<Word[]>([]);
  const [pageInfo, setPageInfo] = useState<PageInfo>({
    pageSize: 0,
    pageCount: 0,
    totalData: 0,
    page: 0,
  });
  const [pageRequest, setPageRequest] = useState<PageRequest>({
    limit: 10,
    page: 1
  });

  const { isLoading, data } = useQuery(['word', pageRequest], ({ queryKey }) => Api.post('/word', queryKey[1]), {});

  useEffect(() => {
    if (data && data.success) {
      setWords(data.payload.list);
      setPageInfo({
        pageCount: data.payload.totalPage,
        pageSize: data.payload.dataPerPage,
        totalData: data.payload.totalData,
        page: data.payload.page,
      });
    }
  }, [data]);

  return (
    <>
      <Head>
        <title>{process.env.APP_NAME + ' - Word'}</title>
      </Head>
      <div className='px-4'>
        <div className='text-xl h-16 flex items-center border-b'>{'Word'}</div>
        <div className='pt-4 flex justify-end'>
          <Link href={{ pathname: '/word/new' }}>
            <a>
              <div className='flex justify-center items-center duration-300 text-purple-400 hover:text-purple-500 h-8 rounded-sm font-semibold px-2 w-full'>
                <IoAddOutline className='mr-2' size={'1.5rem'} />
                <span className=''>{'New'}</span>
              </div>
            </a>
          </Link>
        </div>
        <div className='pt-4'>
          <div className='bg-white w-full shadow rounded-sm'>
            <div className='flex justify-between items-center px-3 h-16'>
              <div className='text-xl'>{''}</div>
              <button className='h-12 w-12 flex justify-center items-center rounded-full' onClick={() => { }}>
                <MdOutlineFilterList className='' size={'1.5em'} />
              </button>
            </div>
            <div className='mb-16'>
              <Table
                columns={column}
                data={words}
                setPageRequest={setPageRequest}
                pageRequest={pageRequest}
                pageInfo={pageInfo}
                isLoading={isLoading}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );

};

(Index as PageWithLayoutType).layout = Main;

export default Index;
