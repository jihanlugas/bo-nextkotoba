import Head from 'next/head';
import Main from '@com/Layout/Main';
import PageWithLayoutType from '@type/layout';
import { MdOutlineFilterList } from 'react-icons/md';
import { RiPencilLine } from 'react-icons/ri';
import { IoAddOutline } from 'react-icons/io5';
import type { Sentence, PageInfo, PageRequest } from '@type/data';
import Link from 'next/link';
import { useState, useEffect, PropsWithChildren } from 'react';
import Table from '@com/Table/Table';
import { CellProps, Column } from 'react-table';
import { WORD_TAG } from '@utils/Constant';
import { Api } from '@lib/Api';
import { useQuery } from 'react-query';


type Props = {
}

const column: Column[] = [
  {
    id: 'sentence',
    Header: 'Sentence',
    Cell: (cell: PropsWithChildren<CellProps<Sentence, any>>) => {
      let sentence = cell.row.original.sentence;
      let sentanceRuby: { ruby: string, rt: string }[] = [];
      cell.row.original.listRuby.forEach((element, key) => {
        let res = sentence.indexOf(element);
        if (res === 0) {
          sentanceRuby.push({
            ruby: sentence.substring(res, res + element.length),
            rt: cell.row.original.listRubyTr[key],
          });
          sentence = sentence.replace(sentence.substring(res, res + element.length), '');
        } else if (res !== -1) {
          sentanceRuby.push(
            {
              ruby: sentence.substring(0, res),
              rt: '',
            },
            {
              ruby: sentence.substring(res, res + element.length),
              rt: cell.row.original.listRubyTr[key],
            },
          );
          sentence = sentence.replace(sentence.substring(0, res), '');
          sentence = sentence.replace(element, '');
        } else {
          sentanceRuby.push(
            {
              ruby: sentence,
              rt: '',
            },
          );
          sentence = sentence.replace(sentence, '');
        }
      });

      if (sentence !== '') {
        sentanceRuby.push(
          {
            ruby: sentence,
            rt: '',
          },
        );
      }

      return (
        <>
          <div className='font-notoSerif whitespace-nowrap text-lg select-text'>
            {sentanceRuby.map((data, key) => {
              return (
                <ruby key={key}>
                  {data.ruby}<rt>{data.rt}</rt>
                </ruby>
              );
            })}
          </div>
        </>
      );
    },
    accessor: 'sentence',
  },
  {
    id: 'meaning',
    Header: 'Meaning',
    Cell: (cell: PropsWithChildren<CellProps<Sentence, any>>) => {
      return (
        <>
          <span className='truncate select-text'>{cell.row.original.meaning}</span>
        </>
      );
    },
    accessor: 'meaning',
  },
  {
    id: 'description',
    Header: 'Description',
    Cell: (cell: PropsWithChildren<CellProps<Sentence, any>>) => {
      return (
        <>
          <span className='truncate'>{cell.row.original.description}</span>
        </>
      );
    },
    accessor: 'description',
  },
  {
    id: 'listKanji',
    Header: 'List Kanji',
    Cell: (cell: PropsWithChildren<CellProps<Sentence, any>>) => {
      return (
        <>
          <div className='font-notoSerif'>
            {cell.row.original.listKanji.join(', ')}
          </div>
        </>
      );
    },
  },
  {
    id: 'sentenceId',
    Header: '',
    Cell: (cell: PropsWithChildren<CellProps<Sentence, any>>) => {
      return (
        <>
          <div className='flex justify-end'>
            <Link href={{ pathname: '/sentence/[sentence]', query: { sentence: cell.row.original.sentenceId } }}>
              <a>
                <div className='ml-2 px-2 duration-300 text-purple-400 hover:text-purple-500 '>
                  <MdOutlineFilterList className='' size={'1.5rem'} />
                </div>
              </a>
            </Link>
            <Link href={{ pathname: '/sentence/[sentence]/edit', query: { sentence: cell.row.original.sentenceId } }}>
              <a>
                <div className='ml-2 px-2 duration-300 text-purple-400 hover:text-purple-500 '>
                  <RiPencilLine className='' size={'1.5rem'} />
                </div>
              </a>
            </Link>
          </div>
        </>
      );
    }
  }
];

const Index: React.FC<Props> = () => {
  const [sentances, setSentances] = useState<Sentence[]>([]);
  const [pageInfo, setPageInfo] = useState<PageInfo>({
    pageSize: 0,
    pageCount: 0,
    totalData: 0,
    page: 0,
  });
  const [pageRequest, setPageRequest] = useState<PageRequest>({
    limit: 10,
    page: 1
  });

  const { isLoading, data } = useQuery(['sentence', pageRequest], ({ queryKey }) => Api.post('/sentence', queryKey[1]), {});

  useEffect(() => {
    if (data && data.success) {
      setSentances(data.payload.list);
      setPageInfo({
        pageCount: data.payload.totalPage,
        pageSize: data.payload.dataPerPage,
        totalData: data.payload.totalData,
        page: data.payload.page,
      });
    }
  }, [data]);

  return (
    <>
      <Head>
        <title>{process.env.APP_NAME + ' - Sentence'}</title>
      </Head>
      <div className='px-4'>
        <div className='text-xl h-16 flex items-center border-b'>
          <span>{'Sentence'}</span>
        </div>
        <div className='pt-4 flex justify-end'>
          <Link href={{ pathname: '/sentence/new' }}>
            <a>
              <div className='flex justify-center items-center duration-300 text-purple-400 hover:text-purple-500 h-8 rounded-sm font-semibold px-2 w-full'>
                <IoAddOutline className='mr-2' size={'1.5rem'} />
                <span className=''>{'New'}</span>
              </div>
            </a>
          </Link>
        </div>
        <div className='pt-4'>
          <div className='bg-white w-full shadow rounded-sm'>
            <div className='flex justify-between items-center px-3 h-16'>
              <div className='text-xl'>{''}</div>
              <button className='h-12 w-12 flex justify-center items-center rounded-full' onClick={() => { }}>
                <MdOutlineFilterList className='' size={'1.5em'} />
              </button>
            </div>
            <div className='mb-16'>
              <Table
                columns={column}
                data={sentances}
                setPageRequest={setPageRequest}
                pageRequest={pageRequest}
                pageInfo={pageInfo}
                isLoading={isLoading}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

(Index as PageWithLayoutType).layout = Main;

export default Index;