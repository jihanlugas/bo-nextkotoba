import Head from 'next/head';
import Main from '@com/Layout/Main';
import PageWithLayoutType from '@type/layout';
import { GetServerSideProps } from 'next/types';
import type { Sentence } from '@type/data';
import { Api } from '@lib/Api';
import { CgChevronRight } from 'react-icons/cg';
import { useRouter } from 'next/router';


type Props = {
  sentence: Sentence
}

const Index: React.FC<Props> = ({ sentence }) => {

  const router = useRouter();

  return (
    <>
      <Head>
        <title>{sentence.sentence + ' - sentence'}</title>
      </Head>
      <div className='px-4'>
        <div className='text-xl h-16 flex items-center border-b'>
          <button type='button' className='' onClick={() => router.push({ pathname: '/sentence' })}>{'Sentence'}</button>
          <CgChevronRight className='mx-2' />
          <span className='' >{sentence.sentence}</span>
        </div>
        <div className='pt-4'>
          <div className='flex mb-4'>
            <div className='flex w-full md:w-2/3'>
              {/* <div className='flex-none'>
                <div className='w-36 h-36 mr-4 flex justify-center items-center bg-white shadow rounded-sm'>
                  <div className='font-yujiSyuku text-7xl'>{sentence.sentence}</div>
                </div>
              </div> */}
              <div className='grow px-4'>
                {/* <div className='grid grid-cols-4 gap-4 mb-4'>
                  <div className='flex flex-col items-start justify-center'>
                    <div>{'Strokes'}</div>
                    <div>{word.strokeCount}</div>
                  </div>
                  <div className='flex flex-col items-start justify-center'>
                    <div>{'Unicode'}</div>
                    <div>{word.unicode}</div>
                  </div>
                  <div className='flex flex-col items-start justify-center'>
                    <div>{'School Grade'}</div>
                    <div>{word.schoolGrade}</div>
                  </div>
                  <div className='flex flex-col items-start justify-center'>
                    <div>{'JLPT Level'}</div>
                    <div>{word.jlptLevel}</div>
                  </div>
                </div>
                <div className='mb-4'>
                  <div className='flex mb-4'>
                    <div className='w-40'>{'Heisig En'}</div>
                    <div>{word.heisigEn}</div>
                  </div>
                  <div className='flex mb-4'>
                    <div className='w-40'>{'List Meaning'}</div>
                    <div>{word.listMeaning}</div>
                  </div>
                  <div className='flex mb-4'>
                    <div className='w-40'>{'Kun Readings'}</div>
                    <div>{word.listKunReading}</div>
                  </div>
                  <div className='flex mb-4'>
                    <div className='w-40'>{'On Readings'}</div>
                    <div>{word.listOnReading}</div>
                  </div>
                  <div className='flex mb-4'>
                    <div className='w-40'>{'Name Reading'}</div>
                    <div>{word.listNameReading}</div>
                  </div>
                </div>
                <div className='mb-4'>
                  <div className='text-xl h-12 flex items-center justify-between bg-purple-300 px-4'>
                    <div>{'Form Explanation'}</div>
                    <div>icon</div>
                  </div>
                </div>
                <div className='mb-4'>
                  <div className='text-xl h-12 flex items-center justify-between bg-purple-300 px-4'>
                    <div>{'Reading Examples'}</div>
                    <div>icon</div>
                  </div>
                </div>
                <div className='mb-4'>
                  <div className='text-xl h-12 flex items-center justify-between bg-purple-300 px-4'>
                    <div>{'Word'}</div>
                    <div>icon</div>
                  </div>
                </div>
                <div className='mb-4'>
                  <div className='text-xl h-12 flex items-center justify-between bg-purple-300 px-4'>
                    <div>{'Sentences'}</div>
                    <div>icon</div>
                  </div>
                </div>
                <div className='mb-4'>
                  <div className='text-xl h-12 flex items-center justify-between bg-purple-300 px-4'>
                    <div>{'Names'}</div>
                    <div>icon</div>
                  </div>
                </div> */}
              </div>
            </div>
            <div className="hidden md:flex w-1/3 mb-4 p-4 whitespace-pre-wrap">
              {JSON.stringify(sentence, null, 4)}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};


(Index as PageWithLayoutType).layout = Main;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { sentence } = context.query;
  const data = await Api.get('/sentence/id/' + sentence).then(res => res);

  if (data.success) {
    return {
      props: {
        sentence: data.payload,
      }
    };
  } else {
    return {
      notFound: true
    };
  }
};

export default Index;