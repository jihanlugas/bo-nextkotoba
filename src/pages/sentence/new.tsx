import Main from '@com/Layout/Main';
import PageWithLayoutType from '@type/layout';
import * as Yup from 'yup';
import { Form, Formik, FormikValues, FieldArray, FastField } from 'formik';
import Head from 'next/head';
import TextField from '@com/Formik/TextField';
import TextAreaField from '@com/Formik/TextAreaField';
import ButtonSubmit from '@com/Formik/ButtonSubmit';
import SelectBox from '@com/Formik/SelectBox';
import { Sentence } from '@type/data';
import { WORD_TAG } from '@utils/Constant';
import { IoCloseOutline } from 'react-icons/io5';
import { useContext, useState } from 'react';
import { IoAddOutline } from 'react-icons/io5';
import { useMutation } from 'react-query';
import { Api } from '@lib/Api';
import NotifContext from '@stores/notifProvider';
import { useRouter } from 'next/router';
import { CgChevronRight } from 'react-icons/cg';
import CheckBox from '@com/Formik/CheckBox';


type Props = {
}

const schema = Yup.object().shape({
  sentence: Yup.string().max(160).required('Required'),
  meaning: Yup.string().max(500).required('Required'),
  description: Yup.string().max(500),
  listRuby: Yup.array().of(
    Yup.string()
      .matches(/^[一-龯]+$/, 'field kanji')
      .required('Required')
  ),
  listRubyTr: Yup.array().of(
    Yup.string()
      .matches(/^[ぁ-んァ-ン.。\s|-]+$/, 'field kana')
      .required('Required')
  ),
});

const New: React.FC<Props> = () => {

  const router = useRouter();

  const { notif } = useContext(NotifContext);

  const initFormikValue: Sentence = {
    sentenceId: 0,
    sentence: '',
    meaning: '',
    description: '',
    listRuby: [],
    listRubyTr: [],
    listKanji: [],
  };

  const { data: dataSubmit, mutate: mutateSubmit, isLoading: isLoadingSubmit } = useMutation((val: FormikValues) => Api.post('/sentence/create', val));

  const handleSubmit = (values: FormikValues, setErrors) => {
    mutateSubmit(values, {
      onSuccess: (res) => {
        if (res) {
          if (res.success) {
            notif.success(res.message);
            router.push('/sentence');
          } else if (res.error) {
            if (res.payload && res.payload.listError) {
              setErrors(res.payload.listError);
            } else {
              notif.error(res.message);
            }
          }
        }
      },
      onError: (res) => {
        notif.error('Please cek you connection');
      },
    });

  };


  return (
    <>
      <Head>
        <title>{process.env.APP_NAME + ' - Sentence New'}</title>
      </Head>
      <div className='px-4'>
        <div className='text-xl h-16 flex items-center border-b'>
          <button type='button' className='' onClick={() => router.push('/sentence')}>{'Sentence'}</button>
          <CgChevronRight className='mx-2' />
          <span className='' >{'New'}</span>
        </div>
        <div className='pt-4'>
          <Formik
            initialValues={initFormikValue}
            validationSchema={schema}
            enableReinitialize={true}
            onSubmit={(values, { setErrors }) => handleSubmit(values, setErrors)}
          >
            {({ values, setValues }) => {
              return (
                <Form>
                  <div className='flex mb-4'>
                    <div className={'w-full md:w-2/3'}>
                      <div className="mb-4">
                        <TextField
                          label={'Sentence'}
                          name={'sentence'}
                          type={'text'}
                          placeholder={'sentence'}
                          required
                        />
                      </div>
                      <div className="mb-4">
                        <TextField
                          label={'Meaning'}
                          name={'meaning'}
                          type={'text'}
                          placeholder={'meaning'}
                          required
                        />
                      </div>
                      <div className="mb-4">
                        <TextAreaField
                          label={'Description'}
                          name={'description'}
                          type={'text'}
                          placeholder={'description'}
                        />
                      </div>
                      <div className="mb-4">
                        <FieldArray
                          name='listRuby'
                          render={arrayHelpers => {
                            return (
                              <div>
                                <div>{'Ruby'}</div>
                                <div className='border p-2'>
                                  <div className='grid grid-cols-1 md:grid-cols-3 gap-4 mb-4'>
                                    {values.listRuby.map((v, key) => (
                                      <div className="flex" key={key}>
                                        <TextField
                                          name={`listRuby.${key}`}
                                          type={'text'}
                                          placeholder={'Ruby'}
                                        />
                                        <button type='button' className={'h-10 w-10 flex justify-center items-center '} onClick={() => arrayHelpers.remove(key)}>
                                          <IoCloseOutline className={'text-red-500'} size={'1.5rem'} />
                                        </button>
                                      </div>
                                    ))}
                                  </div>
                                  <div className='flex justify-center'>
                                    <div className='px-4 select-none'>
                                      <button
                                        className={'duration-300 h-10 font-semibold w-full flex justify-center items-center text-purple-400 hover:text-purple-500'}
                                        onClick={() => arrayHelpers.push('')}
                                        type={'button'}
                                      ><IoAddOutline className='mr-2' size={'1.5em'} /> {'Add'}</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            );
                          }}
                        />
                      </div>
                      <div className="mb-4">
                        <FieldArray
                          name='listRubyTr'
                          render={arrayHelpers => {
                            return (
                              <div>
                                <div>{'Ruby Tr'}</div>
                                <div className='border p-2'>
                                  <div className='grid grid-cols-1 md:grid-cols-3 gap-4 mb-4'>
                                    {values.listRubyTr.map((v, key) => (
                                      <div className="flex" key={key}>
                                        <TextField
                                          name={`listRubyTr.${key}`}
                                          type={'text'}
                                          placeholder={'Ruby Tr'}
                                        />
                                        <button type='button' className={'h-10 w-10 flex justify-center items-center '} onClick={() => arrayHelpers.remove(key)}>
                                          <IoCloseOutline className={'text-red-500'} size={'1.5rem'} />
                                        </button>
                                      </div>
                                    ))}
                                  </div>
                                  <div className='flex justify-center'>
                                    <div className='px-4 select-none'>
                                      <button
                                        className={'duration-300 h-10 font-semibold w-full flex justify-center items-center text-purple-400 hover:text-purple-500'}
                                        onClick={() => arrayHelpers.push('')}
                                        type={'button'}
                                      ><IoAddOutline className='mr-2' size={'1.5em'} /> {'Add'}</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            );
                          }}
                        />
                      </div>
                      <div className={'mb-4'}>
                        <ButtonSubmit
                          label={'Save'}
                          disabled={isLoadingSubmit}
                          loading={isLoadingSubmit}
                        />
                      </div>
                    </div>
                    <div className="hidden md:flex w-1/3 mb-4 p-4 whitespace-pre-wrap">
                      {JSON.stringify(values, null, 4)}
                    </div>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </div>
      </div>
    </>
  );
};

(New as PageWithLayoutType).layout = Main;

export default New;