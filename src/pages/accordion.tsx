import NotifContext from '@stores/notifProvider';
import UserContext from '@stores/userProvider';
import Head from 'next/head';
import { useContext, useRef, useState } from 'react';
import * as Yup from 'yup';
import { Form, Formik, FormikValues } from 'formik';
import { useMutation } from 'react-query';
import TextField from '@com/Formik/TextField';
import ButtonSubmit from '@com/Formik/ButtonSubmit';
import { Api } from '@lib/Api';
import Link from 'next/link';
import { useRouter } from 'next/router';

type Props = {

}


type AccordionProps = {
  title: string
  content: string
}


const Accordion = ({ title, content }: AccordionProps) => {
  const [isOpened, setOpened] = useState<boolean>(false);
  const [height, setHeight] = useState<string>('0px');
  const contentElement = useRef<HTMLDivElement>(null);

  const HandleOpening = () => {
    setOpened(!isOpened);
    setHeight(!isOpened ? `${contentElement.current.scrollHeight}px` : '0px');
  };
  return (
    <div onClick={HandleOpening} className="border border-indigo-400">
      <div className={'bg-indigo-300 p-4 flex justify-between text-white'}>
        <h4 className="font-semibold">{title}</h4>
        {isOpened ? 'Open' : 'Close'}
      </div>
      <div
        ref={contentElement}
        style={{ height: height }}
        className="bg-gray-200 overflow-hidden transition-all duration-200"
      >
        <p className="p-4">{content}</p>
      </div>
    </div>
  );
};

const Index: React.FC<Props> = () => {

  const data = [
    {
      title: 'How much do i need to pay for this?',
      content:
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit nesciunt beatae debitis delectus pariatur nostrum maiores magni quibusdam officia tempore quis ea molestiae ducimus error nemo, tenetur possimus, earum illo.'
    },
    {
      title: 'Can i delete my account?',
      content:
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit nesciunt beatae debitis delectus pariatur nostrum maiores magni quibusdam officia tempore quis ea molestiae ducimus error nemo, tenetur possimus, earum illo. Impedit nesciunt beatae debitis delectus pariatur nostrum maiores magni quibusdam officia tempore quis ea molestiae ducimus error nemo, tenetur possimus, earum illo.'
    },
    {
      title: 'Is this accordion styled in Tailwind css?',
      content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
    }
  ];

  return (
    <>
      <Head>
        <title>{process.env.APP_NAME + ' - Index'}</title>
        <meta name="theme-color" content={'#FAF5FF'} />
      </Head>
      <div className='p-20'>
        <div className="container mx-auto">
          <h1 className="w-full text-center">Accordion</h1>
          {data.map((item, key) => (
            <Accordion key={key} title={item.title} content={item.content} />
          ))}
        </div>
      </div>

    </>
  );
};

export default Index;

