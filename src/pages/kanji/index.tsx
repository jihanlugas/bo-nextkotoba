import Head from 'next/head';
import Main from '@com/Layout/Main';
import PageWithLayoutType from '@type/layout';
import { MdOutlineFilterList } from 'react-icons/md';
import { RiPencilLine } from 'react-icons/ri';
import { IoAddOutline } from 'react-icons/io5';
import type { Kanji, PageInfo, PageRequest } from '@type/data';
import Link from 'next/link';
import { useState, useEffect, PropsWithChildren } from 'react';
import Table from '@com/Table/Table';
import { JLTP_LEVEL, SCHOOL_GRADE } from '@utils/Constant';
import { Api } from '@lib/Api';
import { useQuery } from 'react-query';
import { CellProps, Column } from 'react-table';

type Props = {
}

const column: Column[] = [
  {
    id: 'kanji',
    Header: 'Kanji',
    accessor: 'kanji',
    Cell: (cell: PropsWithChildren<CellProps<Kanji, any>>) => {
      return (
        <>
          <span className='font-notoSerif font-semibold'>
            {cell.row.original.kanji}
          </span>
        </>
      );
    },
  },
  {
    id: 'heisigEn',
    Header: 'Heisig En',
    Cell: (cell: PropsWithChildren<CellProps<Kanji, any>>) => {
      return (
        <>
          <span className='capitalize whitespace-nowrap'>{cell.row.original.heisigEn}</span>
        </>
      );
    },
  },
  {
    id: 'listMeaning',
    Header: 'List Meaning',
    Cell: (cell: PropsWithChildren<CellProps<Kanji, any>>) => {
      return (
        <>
          <div className=''>
            {cell.row.original.listMeaning.join(', ')}
          </div>
        </>
      );
    },
  },
  {
    id: 'listKunReading',
    Header: 'List Kun Reading',
    maxWidth: 60,
    Cell: (cell: PropsWithChildren<CellProps<Kanji, any>>) => {
      return (
        <>
          <div className='font-notoSerif'>
            {cell.row.original.listKunReading.join(', ')}
          </div>
        </>
      );
    },
  },
  {
    id: 'listOnReading',
    Header: 'List On Reading',
    maxWidth: 60,
    Cell: (cell: PropsWithChildren<CellProps<Kanji, any>>) => {
      return (
        <>

          <div className='font-notoSerif'>
            {cell.row.original.listOnReading.join(', ')}
          </div>
        </>
      );
    },
  },
  {
    id: 'listNameReading',
    Header: 'List Name Reading',
    maxWidth: 60,
    Cell: (cell: PropsWithChildren<CellProps<Kanji, any>>) => {
      return (
        <>
          <div className='font-notoSerif'>
            {cell.row.original.listNameReading.join(', ')}
          </div>
        </>
      );
    },
  },
  {
    id: 'strokeCount',
    Header: 'Stroke Count',
    accessor: 'strokeCount',
  },
  // {
  //   id: 'unicode',
  //   Header: 'Unicode',
  //   accessor: 'unicode',
  // },
  {
    id: 'jlptLevel',
    Header: 'Jlpt Level',
    Cell: (cell: PropsWithChildren<CellProps<Kanji, any>>) => {
      return (
        <>
          <span className='whitespace-nowrap'>{JLTP_LEVEL[cell.row.original.jlptLevel].label}</span>
        </>
      );
    },
  },
  {
    id: 'schoolGrade',
    Header: 'School Grade',
    Cell: (cell: PropsWithChildren<CellProps<Kanji, any>>) => {
      return (
        <>
          <span className='whitespace-nowrap'>{SCHOOL_GRADE[cell.row.original.schoolGrade].label}</span>
        </>
      );
    },
  },
  {
    id: 'kanjiId',
    Header: '',
    Cell: (cell: PropsWithChildren<CellProps<Kanji, any>>) => {
      return (
        <>
          <div className='flex justify-end'>
            <div className='mx-2'>
              <Link href={{ pathname: '/kanji/[kanji]', query: { kanji: cell.row.original.kanji } }}>
                <a>
                  <div className='ml-2 px-2 duration-300 text-purple-400 hover:text-purple-500 '>
                    <MdOutlineFilterList className='' size={'1.5rem'} />
                  </div>
                </a>
              </Link>
            </div>
            <div className='mx-2'>
              <Link href={{ pathname: '/kanji/[kanji]/edit', query: { kanji: cell.row.original.kanji } }}>
                <a>
                  <div className='ml-2 px-2 duration-300 text-purple-400 hover:text-purple-500 '>
                    <RiPencilLine className='' size={'1.5rem'} />
                  </div>
                </a>
              </Link>
            </div>
          </div>
        </>
      );
    }
  }
];

const Index: React.FC<Props> = () => {
  const [kanjis, setKanjis] = useState<Kanji[]>([]);

  const [pageInfo, setPageInfo] = useState<PageInfo>({
    pageSize: 0,
    pageCount: 0,
    totalData: 0,
    page: 0,
  });

  const [pageRequest, setPageRequest] = useState<PageRequest>({
    limit: 10,
    page: 1
  });

  const { isLoading, data } = useQuery(['kanji', pageRequest], ({ queryKey }) => Api.post('/kanji', queryKey[1]), {});

  useEffect(() => {
    if (data && data.success) {
      setKanjis(data.payload.list);
      setPageInfo({
        pageCount: data.payload.totalPage,
        pageSize: data.payload.dataPerPage,
        totalData: data.payload.totalData,
        page: data.payload.page,
      });
    }
  }, [data]);


  return (
    <>
      <Head>
        <title>{process.env.APP_NAME + ' - Kanji'}</title>
      </Head>
      <div className='px-4'>
        <div className='text-xl h-16 flex items-center border-b'>{'Kanji'}</div>
        <div className='pt-4 flex justify-end'>
          <Link href={{ pathname: '/kanji/new' }}>
            <a>
              <div className='flex justify-center items-center duration-300 text-purple-400 hover:text-purple-500 h-8 rounded-sm font-semibold px-2 w-full'>
                <IoAddOutline className='mr-2' size={'1.5rem'} />
                <span className=''>{'New'}</span>
              </div>
            </a>
          </Link>
        </div>
        <div className='pt-4'>
          <div className='bg-white w-full shadow rounded-sm'>
            <div className='flex justify-between items-center px-3 h-16'>
              <div className='text-xl'>{''}</div>
              <button className='h-12 w-12 flex justify-center items-center rounded-full' onClick={() => { }}>
                <MdOutlineFilterList className='' size={'1.5em'} />
              </button>
            </div>
            <div className='mb-16'>
              <Table
                columns={column}
                data={kanjis}
                setPageRequest={setPageRequest}
                pageRequest={pageRequest}
                pageInfo={pageInfo}
                isLoading={isLoading}
              />
            </div>
            {/* <table className='w-full table-auto text-sm'>
              <thead>
                <tr className='border-b text-left'>
                  <th className='py-4 px-3 font-normal'>{'Kanji'}</th>
                  <th className='py-4 px-3 font-normal'>{'List Meaning'}</th>
                  <th className='py-4 px-3 font-normal'>{'List Kun Reading'}</th>
                  <th className='py-4 px-3 font-normal'>{'List On Reading'}</th>
                  <th className='py-4 px-3 font-normal'>{'List Name Reading'}</th>
                  <th className='py-4 px-3 font-normal'>{'Stroke Count'}</th>
                  <th className='py-4 px-3 font-normal'>{'Heisig En'}</th>
                  <th className='py-4 px-3 font-normal'>{'Unicode'}</th>
                  <th className='py-4 px-3 font-normal'>{'Jlpt Level'}</th>
                  <th className='py-4 px-3 font-normal'>{'School Grade'}</th>
                  <th className='py-4 px-3 font-normal'>{'Action'}</th>
                </tr>
              </thead>
              <tbody>
                {data.map((v, key) => {
                  return (
                    <tr key={key} className='border-b duration-300 hover:bg-gray-100'>
                      <td className='p-3'>{v.kanji}</td>
                      <td className='p-3'>
                        {v.listMeaning.join(', ')}
                      </td>
                      <td className='p-3 font-notoSerif'>
                        <div className='flex flex-wrap items-center'>
                          {v.listKunReading.map((v, key) => {
                            return (
                              <span key={key} className={'bg-blue-500 font-semibold text-white mr-2 py-1 px-2 rounded-sm my-1'}>{v}</span>
                            );
                          })}
                        </div>
                      </td>
                      <td className='p-3 font-notoSerif'>
                        <div className='flex flex-wrap items-center'>
                          {v.listOnReading.map((v, key) => {
                            return (
                              <span key={key} className={'bg-green-500 font-semibold text-white mr-2 py-1 px-2 rounded-sm my-1'}>{v}</span>
                            );
                          })}
                        </div>
                      </td>
                      <td className='p-3'>{v.listNameReading}</td>
                      <td className='p-3'>{v.strokeCount}</td>
                      <td className='p-3'>{v.heisigEn}</td>
                      <td className='p-3'>{v.unicode}</td>
                      <td className='p-3'>{v.jlptLevel}</td>
                      <td className='p-3'>{v.schoolGrade}</td>
                      <td className='p-3 flex justify-end'>
                        <Link href={{ pathname: '/kanji/[kanjiId]', query: { kanjiId: v.kanjiId } }}>
                          <a>
                            <div className='flex justify-center items-center ml-4 h-8 w-8 shadow rounded-sm duration-300 hover:bg-purple-100'>
                              <MdOutlineFilterList className='' size={'1rem'} />
                            </div>
                          </a>
                        </Link>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table> */}
          </div>
        </div>
      </div>
    </>
  );
};

(Index as PageWithLayoutType).layout = Main;

export default Index;
