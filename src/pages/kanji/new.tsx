import Main from '@com/Layout/Main';
import PageWithLayoutType from '@type/layout';
import * as Yup from 'yup';
import { Form, Formik, FormikValues, FieldArray } from 'formik';
import Head from 'next/head';
import TextField from '@com/Formik/TextField';
import ButtonSubmit from '@com/Formik/ButtonSubmit';
import SelectBox from '@com/Formik/SelectBox';
import { Kanji } from '@type/data';
import { JLTP_LEVEL, SCHOOL_GRADE } from '@utils/Constant';
import { IoCloseOutline } from 'react-icons/io5';
import { useContext, useState } from 'react';
import { IoAddOutline } from 'react-icons/io5';
import { useMutation } from 'react-query';
import { Api } from '@lib/Api';
import NotifContext from '@stores/notifProvider';
import { useRouter } from 'next/router';
import { CgChevronRight } from 'react-icons/cg';

type Props = {
}

const schema = Yup.object().shape({
  kanji: Yup.string().max(1).matches(/^[一-龯]+$/, 'field kanji').required('required field'),
  listMeaning: Yup.array().of(
    Yup.string()
      .required('required field')
  ),
  listKunReading: Yup.array().of(
    Yup.string()
      .matches(/^[ぁ-ん.。\s|-]+$/, 'field hiragana')
      .required('kun reading is a required field')
  ),
  listOnReading: Yup.array().of(
    Yup.string()
      .matches(/^[ァ-ン.。\s|-]+$/, 'field katakana')
      .required('on reading is a required field')
  ),
  listNameReading: Yup.array().of(
    Yup.string()
      .matches(/^[ぁ-んァ-ン.。\s|-]+$/, 'field kana')
      .required('name reading is a required field')
  ),
  strokeCount: Yup.string().required(),
  heisigEn: Yup.string().required(),
  unicode: Yup.string(),
  jlptLevel: Yup.string().oneOf(Object.keys(JLTP_LEVEL)).required(),
  schoolGrade: Yup.string().oneOf(Object.keys(SCHOOL_GRADE)).required(),
});


const New: React.FC<Props> = () => {

  const router = useRouter();

  const { notif } = useContext(NotifContext);

  const initFormikValue: Kanji = {
    kanjiId: 0,
    kanji: '',
    listMeaning: [],
    listKunReading: [],
    listOnReading: [],
    listNameReading: [],
    strokeCount: 0,
    heisigEn: '',
    unicode: '',
    jlptLevel: '',
    schoolGrade: '',
  };

  const { data: dataSubmit, mutate: mutateSubmit, isLoading: isLoadingSubmit } = useMutation((val: FormikValues) => Api.post('/kanji/create', val));

  const handleSubmit = (values: FormikValues, setErrors) => {
    mutateSubmit(values, {
      onSuccess: (res) => {
        if (res) {
          if (res.success) {
            notif.success(res.message);
            router.push('/kanji');
          } else if (res.error) {
            if (res.payload && res.payload.listError) {
              setErrors(res.payload.listError);
            } else {
              notif.error(res.message);
            }
          }
        }
      },
      onError: (res) => {
        notif.error('Please cek you connection');
      },
    });
  };

  return (
    <>
      <Head>
        <title>{process.env.APP_NAME + ' - Kanji New'}</title>
      </Head>
      <div className='px-4'>
        <div className='text-xl h-16 flex items-center border-b'>
          <button type='button' className='' onClick={() => { }}>{'Kanji'}</button>
          <CgChevronRight className='mx-2' />
          <span className='' >{'New'}</span>
        </div>
        <div className='pt-4'>
          <Formik
            initialValues={initFormikValue}
            validationSchema={schema}
            enableReinitialize={true}
            onSubmit={(values, { setErrors }) => handleSubmit(values, setErrors)}
          >
            {({ values, setValues }) => {
              return (
                <Form>
                  <div className='flex mb-4'>
                    <div className={'w-full md:w-2/3'}>
                      <div className="mb-4">
                        <TextField
                          label={'Kanji'}
                          name={'kanji'}
                          type={'text'}
                          placeholder={'Kanji'}
                          required
                        />
                      </div>
                      <div className="mb-4">
                        <TextField
                          label={'Heisig En'}
                          name={'heisigEn'}
                          type={'text'}
                          placeholder={'Heisig En'}
                          required
                        />
                      </div>
                      <div className="mb-4">
                        <TextField
                          label={'Stroke Count'}
                          name={'strokeCount'}
                          type={'number'}
                          placeholder={'Stroke Count'}
                          required
                        />
                      </div>
                      <div className="mb-4">
                        <TextField
                          label={'Unicode'}
                          name={'unicode'}
                          type={'text'}
                          placeholder={'Unicode'}
                        />
                      </div>
                      <div className="mb-4">
                        <SelectBox
                          label={'JLPT Level'}
                          name={'jlptLevel'}
                          listData={Object.values(JLTP_LEVEL)}
                          dataLabel={'label'}
                          dataValue={'key'}
                          placeholder={'Select'}
                          required
                        />
                      </div>
                      <div className="mb-4">
                        <SelectBox
                          label={'School Grade'}
                          name={'schoolGrade'}
                          listData={Object.values(SCHOOL_GRADE)}
                          dataLabel={'label'}
                          dataValue={'key'}
                          placeholder={'Select'}
                          required
                        />
                      </div>
                      <div className="mb-4">
                        <FieldArray
                          name='listMeaning'
                          render={arrayHelpers => {
                            return (
                              <div>
                                <div>{'List Meaning'}</div>
                                <div className='border p-2'>
                                  <div className='grid grid-cols-1 md:grid-cols-3 gap-4 mb-4'>
                                    {values.listMeaning.map((v, key) => (
                                      <div className="flex" key={key}>
                                        <TextField
                                          name={`listMeaning.${key}`}
                                          type={'text'}
                                          placeholder={'Meaning'}
                                        />
                                        <button type='button' className={'h-10 w-10 flex justify-center items-center '} onClick={() => arrayHelpers.remove(key)}>
                                          <IoCloseOutline className={'text-red-500'} size={'1.5rem'} />
                                        </button>
                                      </div>
                                    ))}
                                  </div>
                                  <div className='flex justify-center'>
                                    <div className='px-4 select-none'>
                                      <button
                                        className={'duration-300 h-10 font-semibold w-full flex justify-center items-center text-purple-400 hover:text-purple-500'}
                                        onClick={() => arrayHelpers.push('')}
                                        type={'button'}
                                      ><IoAddOutline className='mr-2' size={'1.5em'} /> {'Add'}</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            );
                          }}
                        />
                      </div>
                      <div className="mb-4">
                        <FieldArray
                          name='listKunReading'
                          render={arrayHelpers => {
                            return (
                              <div>
                                <div>{'List Kun Reading'}</div>
                                <div className='border p-2'>
                                  <div className='grid grid-cols-1 md:grid-cols-3 gap-4 mb-4'>
                                    {values.listKunReading.map((v, key) => (
                                      <div className="flex" key={key}>
                                        <TextField
                                          name={`listKunReading.${key}`}
                                          type={'text'}
                                          placeholder={'Kun Reading'}
                                        />
                                        <button type='button' className={'h-10 w-10 flex justify-center items-center '} onClick={() => arrayHelpers.remove(key)}>
                                          <IoCloseOutline className={'text-red-500'} size={'1.5rem'} />
                                        </button>
                                      </div>
                                    ))}
                                  </div>
                                  <div className='flex justify-center'>
                                    <div className='px-4 select-none'>
                                      <button
                                        className={'duration-300 h-10 font-semibold w-full flex justify-center items-center text-purple-400 hover:text-purple-500'}
                                        onClick={() => arrayHelpers.push('')}
                                        type={'button'}
                                      ><IoAddOutline className='mr-2' size={'1.5em'} /> {'Add'}</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            );
                          }}
                        />
                      </div>
                      <div className="mb-4">
                        <FieldArray
                          name='listOnReading'
                          render={arrayHelpers => {
                            return (
                              <div>
                                <div>{'List On Reading'}</div>
                                <div className='border p-2'>
                                  <div className='grid grid-cols-1 md:grid-cols-3 gap-4 mb-4'>
                                    {values.listOnReading.map((v, key) => (
                                      <div className="flex" key={key}>
                                        <TextField
                                          name={`listOnReading.${key}`}
                                          type={'text'}
                                          placeholder={'On Reading'}
                                        />
                                        <button type='button' className={'h-10 w-10 flex justify-center items-center '} onClick={() => arrayHelpers.remove(key)}>
                                          <IoCloseOutline className={'text-red-500'} size={'1.5rem'} />
                                        </button>
                                      </div>
                                    ))}
                                  </div>
                                  <div className='flex justify-center'>
                                    <div className='px-4 select-none'>
                                      <button
                                        className={'duration-300 h-10 font-semibold w-full flex justify-center items-center text-purple-400 hover:text-purple-500'}
                                        onClick={() => arrayHelpers.push('')}
                                        type={'button'}
                                      ><IoAddOutline className='mr-2' size={'1.5em'} /> {'Add'}</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            );
                          }}
                        />
                      </div>
                      <div className="mb-4">
                        <FieldArray
                          name='listNameReading'
                          render={arrayHelpers => {
                            return (
                              <div>
                                <div>{'List Name Reading'}</div>
                                <div className='border p-2'>
                                  <div className='grid grid-cols-1 md:grid-cols-3 gap-4 mb-4'>
                                    {values.listNameReading.map((v, key) => (
                                      <div className="flex" key={key}>
                                        <TextField
                                          name={`listNameReading.${key}`}
                                          type={'text'}
                                          placeholder={'Name Reading'}
                                        />
                                        <button type='button' className={'h-10 w-10 flex justify-center items-center '} onClick={() => arrayHelpers.remove(key)}>
                                          <IoCloseOutline className={'text-red-500'} size={'1.5rem'} />
                                        </button>
                                      </div>
                                    ))}
                                  </div>
                                  <div className='flex justify-center'>
                                    <div className='px-4 select-none'>
                                      <button
                                        className={'duration-300 h-10 font-semibold w-full flex justify-center items-center text-purple-400 hover:text-purple-500'}
                                        onClick={() => arrayHelpers.push('')}
                                        type={'button'}
                                      ><IoAddOutline className='mr-2' size={'1.5em'} /> {'Add'}</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            );
                          }}
                        />
                      </div>
                      <div className={'mb-4'}>
                        <ButtonSubmit
                          label={'Save'}
                          disabled={isLoadingSubmit}
                          loading={isLoadingSubmit}
                        />
                      </div>
                    </div>
                    <div className="hidden md:flex w-1/3 mb-4 p-4 whitespace-pre-wrap">
                      {JSON.stringify(values, null, 4)}
                    </div>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </div>
      </div>
    </>
  );
};

(New as PageWithLayoutType).layout = Main;

export default New;