import Head from 'next/head';
import Link from 'next/link';
import Main from '@com/Layout/Main';
import PageWithLayoutType from '@type/layout';
import { GetServerSideProps } from 'next/types';
import type { KanjiDetail, Sentence } from '@type/data';
import { Api } from '@lib/Api';
import { CgChevronUp, CgChevronRight } from 'react-icons/cg';
import { useRouter } from 'next/router';
import { JLTP_LEVEL, SCHOOL_GRADE } from '@utils/Constant';
import { RiPencilLine } from 'react-icons/ri';
import { useRef, useState } from 'react';


type Props = {
  kanji: KanjiDetail
}

type sentenceProps = {
  sentence: Sentence
}

type AccordionProps = {
  title: string
}

const SenteceComp: React.FC<sentenceProps> = ({ sentence }) => {

  let sentenceData = sentence.sentence;
  let sentanceRuby: { ruby: string, rt: string }[] = [];

  sentence.listRuby.forEach((element, key) => {
    let res = sentenceData.indexOf(element);
    if (res === 0) {
      sentanceRuby.push({
        ruby: sentenceData.substring(res, res + element.length),
        rt: sentence.listRubyTr[key],
      });
      sentenceData = sentenceData.replace(sentenceData.substring(res, res + element.length), '');
    } else if (res !== -1) {
      sentanceRuby.push(
        {
          ruby: sentenceData.substring(0, res),
          rt: '',
        },
        {
          ruby: sentenceData.substring(res, res + element.length),
          rt: sentence.listRubyTr[key],
        },
      );
      sentenceData = sentenceData.replace(sentenceData.substring(0, res), '');
      sentenceData = sentenceData.replace(element, '');
    } else {
      sentanceRuby.push(
        {
          ruby: sentenceData,
          rt: '',
        },
      );
      sentenceData = sentenceData.replace(sentenceData, '');
    }
  });

  return (
    <>
      <div className='p-4 mb-1'>
        <Link href={{ pathname: '/sentence/[sentence]', query: { sentence: sentence.sentenceId } }}>
          <a>
            <div className='font-notoSerif whitespace-nowrap text-lg select-text'>
              {sentanceRuby.map((data, key) => {
                return (
                  <ruby key={key}>
                    {data.ruby}<rt>{data.rt}</rt>
                  </ruby>
                );
              })}
            </div>
            <div className='text-sm'>{sentence.meaning}</div>
          </a>
        </Link>
      </div>
    </>
  );
};

const Accordion: React.FC<AccordionProps> = ({ title, children }) => {
  const [isOpened, setOpened] = useState<boolean>(false);
  const [height, setHeight] = useState<string>('0px');
  const contentElement = useRef<HTMLDivElement>(null);

  const HandleOpening = () => {
    setOpened(!isOpened);
    setHeight(!isOpened ? `${contentElement.current.scrollHeight}px` : '0px');
  };

  return (
    <>
      <div onClick={HandleOpening} className="">
        <div className={'bg-purple-400 p-4 flex justify-between text-white'}>
          <h4 className="font-semibold">{title}</h4>
          <CgChevronUp className={`duration-300 ${isOpened && ' rotate-180'}`} size={'1.5rem'} />
        </div>
        <div
          ref={contentElement}
          style={{ height: height }}
          className="overflow-hidden duration-300 shadow"
        >
          {children}
        </div>
      </div>
    </>
  );
};

const Index: React.FC<Props> = ({ kanji }) => {

  const router = useRouter();

  return (
    <>
      <Head>
        <title>{kanji.kanji + ' - Kanji'}</title>
      </Head>
      <div className='px-4'>
        <div className='text-xl h-16 flex items-center border-b'>
          <button type='button' className='' onClick={() => router.push({ pathname: '/kanji' })}>{'Kanji'}</button>
          <CgChevronRight className='mx-2' />
          <span className='' >{kanji.kanji}</span>
        </div>
        <div className='pt-4'>
          <div className='flex mb-4'>
            <div className='block md:flex w-full md:w-2/3'>
              <div className='flex justify-between flex-none mb-8'>
                <div className='w-24 h-24 md:w-36 md:h-36 mr-4 flex justify-center items-center bg-white shadow rounded-sm'>
                  <div className='font-yujiSyuku text-7xl'>{kanji.kanji}</div>
                </div>
                <div className='md:hidden mb-4'>
                  <Link href={{ pathname: '/kanji/[kanji]/edit', query: { kanji: kanji.kanji } }}>
                    <a>
                      <div className='flex justify-center items-center duration-300 text-purple-400 hover:text-purple-500 h-8 rounded-sm font-semibold px-2 w-full'>
                        <RiPencilLine className='mr-2' size={'1.5rem'} />
                        <span className=''>{'Edit'}</span>
                      </div>
                    </a>
                  </Link>
                </div>
              </div>
              <div className='grow px-0 md:px-4'>
                <div className='hidden md:flex justify-end mb-4'>
                  <Link href={{ pathname: '/kanji/[kanji]/edit', query: { kanji: kanji.kanji } }}>
                    <a>
                      <div className='flex justify-center items-center duration-300 text-purple-400 hover:text-purple-500 h-8 rounded-sm font-semibold px-2 w-full'>
                        <RiPencilLine className='mr-2' size={'1.5rem'} />
                        <span className=''>{'Edit'}</span>
                      </div>
                    </a>
                  </Link>
                </div>
                <div className='grid grid-cols-2 md:grid-cols-4 gap-4 mb-4'>
                  <div className='flex flex-col items-start justify-center'>
                    <div>{'Strokes'}</div>
                    <div>{kanji.strokeCount}</div>
                  </div>
                  <div className='flex flex-col items-start justify-center'>
                    <div>{'Unicode'}</div>
                    <div>{kanji.unicode}</div>
                  </div>
                  <div className='flex flex-col items-start justify-center'>
                    <div>{'School Grade'}</div>
                    <div>{SCHOOL_GRADE[kanji.schoolGrade].label}</div>
                  </div>
                  <div className='flex flex-col items-start justify-center'>
                    <div>{'JLPT Level'}</div>
                    <div>{JLTP_LEVEL[kanji.jlptLevel].label}</div>
                  </div>
                </div>
                <hr className='mb-4' />
                <div className='mb-4'>
                  <div className='flex mb-4'>
                    <div className='w-40'>{'Heisig En'}</div>
                    <div className='font-semibold capitalize'>{kanji.heisigEn}</div>
                  </div>
                  <div className='mb-4'>
                    <div className='mb-2'>{'List Meaning'}</div>
                    <div>{kanji.listMeaning.join(', ')}</div>
                  </div>
                  <div className='mb-4'>
                    <div className='mb-2'>{'Kun Readings'}</div>
                    <div className='flex flex-wrap font-notoSerif'>
                      {kanji.listKunReading.map((data, key) => {
                        return (
                          <div key={key} className={'px-2 py-1 mb-2 bg-green-400 rounded mr-2 text-white font-semibold'}>
                            {data}
                          </div>
                        );
                      })}
                    </div>
                  </div>
                  <div className='mb-4'>
                    <div className='mb-2'>{'On Readings'}</div>
                    <div className='flex flex-wrap font-notoSerif'>
                      {kanji.listOnReading.map((data, key) => {
                        return (
                          <div key={key} className={'px-2 py-1 bg-green-400 rounded mr-2 text-white font-semibold'}>
                            {data}
                          </div>
                        );
                      })}
                    </div>
                  </div>
                  <div className='mb-4'>
                    <div className='mb-2'>{'Name Reading'}</div>
                    <div className='flex flex-wrap font-notoSerif'>
                      {kanji.listNameReading.map((data, key) => {
                        return (
                          <div key={key} className={'px-2 py-1 bg-green-400 rounded mr-2 text-white font-semibold'}>
                            {data}
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </div>
                <div className='mb-4'>
                  <Accordion title={'List Word'}>
                    <>
                      {kanji.listWord.length > 0 ? (
                        <>
                          {kanji.listWord.map((data, key) => {
                            return (
                              <div key={key} className='mb-4'>
                                <Link href={{ pathname: '/word/[word]', query: { word: data.word } }}>
                                  <a>
                                    <div className='shadow p-4 mb-4'>
                                      <div className='font-notoSerif'>{data.word}</div>
                                      <div className='text-sm'>{data.listMeaning.join(', ')}</div>
                                    </div>
                                  </a>
                                </Link>
                              </div>
                            );
                          })}
                        </>
                      ) : (
                        <>
                          <div className='p-4'>-</div>
                        </>
                      )}
                    </>
                  </Accordion>
                </div>
                <div className='mb-4'>
                  <Accordion title={'List Sentence'}>
                    <>
                      {kanji.listSentence.length > 0 ? (
                        <>
                          {kanji.listSentence.map((data, key) => {
                            return (
                              <SenteceComp key={key} sentence={data} />
                            );
                          })}
                        </>
                      ) : (
                        <>
                          <div className='p-4'>-</div>
                        </>
                      )}
                    </>
                  </Accordion>
                </div>
              </div>
            </div>
            <div className="hidden md:flex w-1/3 mb-4 p-4 whitespace-pre-wrap">
              {JSON.stringify(kanji, null, 4)}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

(Index as PageWithLayoutType).layout = Main;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { kanji } = context.query;
  const data = await Api.get('/kanji/kanji/detail/' + kanji).then(res => res);

  if (data.success) {
    return {
      props: {
        kanji: data.payload,
      }
    };
  } else {
    return {
      notFound: true
    };
  }
};

export default Index;