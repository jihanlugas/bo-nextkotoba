import { createContext, useState } from 'react';


type Props = {

}

type AppContextState = {
  email: string;
  username: string;
  fullname: string;
  userId: number;
}

const initUser = {
  email: '',
  username: '',
  fullname: '',
  userId: 0,
};

type UserContextType = {
  user: AppContextState,
  setUser: (AppContextState) => void,
}

const UserContext = createContext<UserContextType>({
  user: null,
  setUser: (state: AppContextState) => { },
});

export const UserContextProvider: React.FC<Props> = ({ children }) => {

  const [user, setUser] = useState<AppContextState>(null);

  const context = {
    user,
    setUser,
  };

  return (
    <UserContext.Provider value={context}>
      {children}
    </UserContext.Provider>
  );
};


export default UserContext;