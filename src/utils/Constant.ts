import type { JlptLevel, SchoolGrade, WordTag } from '@type/constant';

export const JLTP_LEVEL: { [key: string]: JlptLevel } = {
  'JLPT_N1': {
    id: 1,
    label: 'JLPT N1',
    key: 'JLPT_N1',
  },
  'JLPT_N2': {
    id: 2,
    label: 'JLPT N2',
    key: 'JLPT_N2',
  },
  'JLPT_N3': {
    id: 3,
    label: 'JLPT N3',
    key: 'JLPT_N3',
  },
  'JLPT_N4': {
    id: 4,
    label: 'JLPT N4',
    key: 'JLPT_N4',
  },
  'JLPT_N5': {
    id: 5,
    label: 'JLPT N5',
    key: 'JLPT_N5',
  },
  'OTHER': {
    id: 6,
    label: 'Other',
    key: 'OTHER',
  },
};

export const SCHOOL_GRADE: { [key: string]: SchoolGrade } = {
  'ELEMENTARY_1': {
    id: 1,
    label: 'Elementary 1',
    key: 'ELEMENTARY_1',
  },
  'ELEMENTARY_2': {
    id: 2,
    label: 'Elementary 2',
    key: 'ELEMENTARY_2',
  },
  'ELEMENTARY_3': {
    id: 3,
    label: 'Elementary 3',
    key: 'ELEMENTARY_3',
  },
  'ELEMENTARY_4': {
    id: 4,
    label: 'Elementary 4',
    key: 'ELEMENTARY_4',
  },
  'ELEMENTARY_5': {
    id: 5,
    label: 'Elementary 5',
    key: 'ELEMENTARY_5',
  },
  'ELEMENTARY_6': {
    id: 6,
    label: 'Elementary 6',
    key: 'ELEMENTARY_6',
  },
  'SECONDARY_1': {
    id: 7,
    label: 'Secondary 1',
    key: 'SECONDARY_1',
  },
  'SECONDARY_2': {
    id: 8,
    label: 'Secondary 2',
    key: 'SECONDARY_2',
  },
  'SECONDARY_3': {
    id: 9,
    label: 'Secondary 3',
    key: 'SECONDARY_3',
  },
  'SECONDARY_4': {
    id: 10,
    label: 'Secondary 4',
    key: 'SECONDARY_4',
  },
  'SECONDARY_5': {
    id: 11,
    label: 'Secondary 5',
    key: 'SECONDARY_5',
  },
  'SECONDARY_6': {
    id: 12,
    label: 'Secondary 6',
    key: 'SECONDARY_6',
  },
  'ADVENCED': {
    id: 13,
    label: 'Advenced',
    key: 'ADVENCED',
  },
  'OTHER': {
    id: 14,
    label: 'Other',
    key: 'OTHER',
  },
};


export const WORD_TAG: { [key: string]: WordTag } = {
  // 'COMMON': {
  //   id: 1,
  //   label: 'Cummon',
  //   key: 'COMMON',
  // },
  'JLPT_N5_VOCAB': {
    id: 2,
    label: 'JLPT N5 Vocab',
    key: 'JLPT_N5_VOCAB',
  },
  'JLPT_N4_VOCAB': {
    id: 3,
    label: 'JLPT N4 Vocab',
    key: 'JLPT_N4_VOCAB',
  },
  'JLPT_N3_VOCAB': {
    id: 4,
    label: 'JLPT N3 Vocab',
    key: 'JLPT_N3_VOCAB',
  },
  'JLPT_N2_VOCAB': {
    id: 5,
    label: 'JLPT N2 Vocab',
    key: 'JLPT_N2_VOCAB',
  },
  'JLPT_N1_VOCAB': {
    id: 6,
    label: 'JLPT N1 Vocab',
    key: 'JLPT_N1_VOCAB',
  },
  'OTHER': {
    id: 7,
    label: 'Other',
    key: 'OTHER',
  },
};