export type JlptLevel = {
  id: number,
  label: string,
  key: string,
}

export type SchoolGrade = {
  id: number,
  label: string,
  key: string,
}

export type WordTag = {
  id: number,
  label: string,
  key: string,
}