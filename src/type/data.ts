export type Kanji = {
  kanjiId: number
  kanji: string
  listMeaning: string[]
  listKunReading: string[]
  listOnReading: string[]
  listNameReading: string[]
  strokeCount: number
  heisigEn: string
  unicode: string
  jlptLevel: string
  schoolGrade: string
}

export type Word = {
  wordId: number
  word: string
  wordTag: string
  listWordRuby: string[]
  listMeaning: string[]
  listKanji: string[]
  isSingleruby: boolean
  isCommon: boolean
}

export type Sentence = {
  sentenceId: number
  sentence: string
  meaning: string
  description: string
  listRuby: string[]
  listRubyTr: string[]
  listKanji: string[]
}

export type KanjiDetail = {
  kanjiId: number
  kanji: string
  listMeaning: string[]
  listKunReading: string[]
  listOnReading: string[]
  listNameReading: string[]
  strokeCount: number
  heisigEn: string
  unicode: string
  jlptLevel: string
  schoolGrade: string
  listWord: Word[]
  listSentence: Sentence[]
}

export type PageInfo = {
  pageSize: number
  pageCount: number
  totalData: number
  page: number
}

export type PageRequest = {
  page: number
  limit: number
}
